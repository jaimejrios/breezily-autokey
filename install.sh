autokey_path=~/.config/autokey
autokey_data_path=~/.config/autokey/data
autokey_scripts_path=~/.config/autokey/data/autokey-hyper-scripts
autokey_hyper_scripts=./autokey-hyper-scripts

copy_autokey_scripts() {

  sleep 2s
  echo "Installing the Breezily Autokey scripts now..."
  sleep 2s
  echo "First, checking if '$autokey_path' directory exists."
  sleep 2s

  if [ -d $autokey_path ] ; then

    echo "> '$autokey_path' was found! Examining..."
    sleep 2s
    echo "\nNow checking if '$autokey_data_path' directory exists..."
    sleep 2s

    if [ -d $autokey_data_path ] ; then

      echo "> '$autokey_data_path' was found!"
      sleep 1s
      echo "> Seeing if '$autokey_scripts_path' exist..."
      sleep 2s

      if [ -d $autokey_scripts_path ]; then

        echo "> All Autokey hyper scripts were found!"
        sleep 1s
        echo "> Moving on..."
      else

        echo "> '$autokey_scripts_path' was not found..."
        sleep 2s
        echo "\nCopying Autokey scripts into $autokey_data_path"
        cp -r $autokey_hyper_scripts $autokey_data_path
        sleep 2s
        echo "Done!"

      fi

    else
      echo "> '$autokey_data_path' was not found..."
      sleep 2s
      echo "Please launch the Autokey program at least once."
      sleep 1s
      echo "This will create the required '~/.config/autokey/data' folder."
      sleep 2s
      echo "Afterwards, re-run this install script to copy the Autokey hyper scripts."
    fi

  else
    echo "> '$autokey_path' directory was not found..."
    sleep 2s
    echo "Please run the command 'sudo apt install autokey-gtk' or 'sudo install autokey-qt'"
    sleep 2s
    echo "Then launch the Autokey program."
    sleep 1s
    echo "Afterwards, re-run this install script to copy the Autokey hyper scripts."
  fi

}

xkb_symbols_path=/usr/share/X11/xkb/symbols
xkb_symbols_file=/usr/share/X11/xkb/symbols/pc

remap_caps_lock_to_hyper() {
  sleep 2s
  echo "\nRemapping the Caps_Lock key to Hyper now..."
  sleep 2s
  echo "First, checking if '$xkb_symbols_path' directory exists."
  sleep 2s

  if [ -d $xkb_symbols_path ]; then
    echo "> '$xkb_symbols_path' was found! Examining..."
    sleep 2s
    echo "\nNow checking if the Caps_Lock has been remapped inside '$xkb_symbols_file'..."
    sleep 2s

    if grep -q Hyper_L "$xkb_symbols_file"; then
      echo "> The Caps_Lock key has already been set to Hyper."
      sleep 1s
      echo "> Moving on!"
    else
      echo "> The Caps_Lock key has not been remapped to Hyper."
      sleep 2s
      echo "> Making these changes now!"
      sleep 2s
      echo "\nFirst, let's back up the xkb default symbol settings to the home '~/' directory."
      sudo cp $xkb_symbols_file ~/.xkb-symbols-backup
      sleep 2s
      echo "Done! DO NOT delete the '~/.xkb-symbols-backup' file."
      sleep 2s
      echo "\nNow remapping the Caps_Lock to Hyper inside '$xkb_symbols_file'"
      sed -i '/modifier_map Lock/d' $xkb_symbols_file
      sed -i 's/Caps_Lock/Hyper_L/g' $xkb_symbols_file
      sleep 2s
      echo "Done!"
    fi

  else
    echo "> Whoops! The '$xkb_symbols_path' directory was not found..."
    sleep 2s
    echo "\nChanges were not made to the Caps_Lock key..."
  fi

}

install_xbindkeys_xdotool() {
  sudo apt install xbindkeys xdotool
  wait
}

copy_xbindkeysrc_file() {
  sleep 2s
  echo "\nNow copying '.xbindkeysrc' file into your home '~/' directory."
  cp ./xbindkeysrc ~/
  sleep 2s
  echo "Done!"
}

ending_msg() {
  sleep 2s
  echo "Make sure to logout and log back in for the changes to take effect."
  sleep 2s
  echo "The install script has completed."
}

exit_cmd() {
  sleep 2s
  echo "Now exiting..."
  sleep 1s
  exit
}

echo "\nWelcome to the Breezily install script!"
echo -n "\nDo you want to continue? [y,N]\n"
read answer

if [ "$answer" != "${answer#[Yy]}" ]; then
  echo "\nOkay! Here are your install options..."
  sleep 2s

  echo "\n- Option 1"
  echo "  > Install all Breezily Autokey scripts."
  echo "  > Remap Caps_Lock with the Hyper_L key."

  echo "\n- Option 2"
  echo "  > Install 'xbindkeys' and 'xdotool' debian packages."
  echo "  > Add the .xbindkeysrc file to your home '~/' directory."

  echo "\n3) Option 3: Full Install"
  echo "  - Includes both Options 1 and 2."

  echo "\n0) Exit install script."

  echo "\nPlease enter in your option below:"
  read option
  if [ "$option" = "1" ]; then
    echo "\nYou chose option $option..."
    copy_autokey_scripts
    remap_caps_lock_to_hyper
    ending_msg
    exit_cmd

  elif [ "$option" = "2" ]; then
    install_xbindkeys_xdotool
    copy_xbindkeysrc_file
    ending_msg
    exit_cmd

  elif [ "$option" = "3" ]; then
    echo "\nYou chose option $option..."
    copy_autokey_scripts
    remap_caps_lock_to_hyper
    install_xbindkeys_xdotool
    copy_xbindkeysrc_file
    ending_msg
    exit_cmd
  else
    exit_cmd
  fi

else
  exit_cmd
fi
